# -*- coding: utf-8 -*- 

import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import pandas as pd
import plotly.graph_objs as go
import json
import numpy as np

external_stylesheets = ["https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css",
                        "//fonts.googleapis.com/css?family=Raleway:400,300,600",
                        "//fonts.googleapis.com/css?family=Dosis:Medium",
                        "https://cdn.rawgit.com/plotly/dash-app-stylesheets/62f0eb4f1fadbefea64b2404493079bf848974e8/dash-uber-ride-demo.css",
                        "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

df = pd.read_csv('data2.csv')
df['Month'] = df['vreme'].map(lambda x: str(x)[8:10]).astype(int)
df['Hour'] = df['vreme'].map(lambda x: str(x)[11:13]).astype(int)

mac_address = ['10:CD:B6:02:70:87', 'E4:34:93:70:8F:70', '50:A7:2B:F8:53:10', '60:83:34:F6:37:36',
               '50:04:B8:71:D9:33', 'DC:CF:96:17:FC:80', '8C:83:E1:04:5F:D8', 'B4:CE:F6:80:3F:41',
               'A4:6C:F1:99:E5:CD', '1C:15:1F:A3:D5:5E', '5C:51:81:BC:0E:37', '54:DC:1D:73:5F:44',
               ]

keys = ['Stanislav Stošić', 'Branislav Čarović', 'Mateja Stevanović', 'Aleksandra Dinčić',
        'Nikola Nedeljković', 'Slobodanka Tešmanović', 'Marija Cvetković', 'Kristina Todorović',
        'Neda Stošić', 'Miloš Nikić', 'Ana Ristić', 'Nikola Obradovic'
        ]

opt = dict(zip(keys, mac_address))
available_indicators = df['mac'].unique()

app.layout = html.Div([
    html.Div([
        html.Div([
            dcc.Dropdown(
                id='crossfilter-yaxis-column',
                options=[{'label': k, 'value': v} for k, v in opt.items()],
                value='E4:34:93:70:8F:70',
                multi=True,
            )
        ], style={'width': '100%', 'float': 'right', 'display': 'inline-block'})
    ], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 50px 50px 50px'
    }),
    html.Div([
        dcc.Graph(
            id='crossfilter-indicator-scatter', # map graph
            hoverData={'points': [{'customdata': '5ced50f0b9a5f'}]}
        )
    ], style={'width': '50%', 'display': 'inline-block', 'padding': '20 0 0 20', 'backgroundColor': 'black'}),

    html.Div([
        dcc.Graph(id='y-time-series'), # second graph
        dcc.Graph(id='x-time-series'), # first graph
        dcc.Graph(id='moji_grafik'),
    ], style={'display': 'inline-block', 'align-items': 'center', 'width': '45%', 'padding': '20 0 0 20',
              'backgroundColor': 'black', }),

    html.Div(dcc.Slider(
        id='crossfilter-year--slider',
        min=1,
        max=30,
        step=1,
        value=8,
        marks={str(year): str(year) for year in range(1, 31)}
    ), style={'width': '90%', 'padding': '50px 0px 0px 50px', 'margin': '0 0 10 20'}),

    html.Div(
        dash_table.DataTable(
            id='dash-table',
            columns=[
                {"name": i, "id": i} for i in sorted(df[['_id', 'mac', 'lat', 'lon', 'vreme']])
            ],
            data=df[['_id', 'mac', 'lat', 'lon', 'vreme']].to_dict('records'),
            row_selectable='single',
            style_header={'backgroundColor': 'rgb(30, 30, 30)'},
            style_cell={
                'backgroundColor': 'rgb(50, 50, 50)',
                'color': 'white'
            },
        ), style={'width': '90%', 'padding': '50px 50px 50px 50px', 'margin': '0 0 10 20'}

    ),

])


@app.callback(
    dash.dependencies.Output('dash-table', 'data'),
    [dash.dependencies.Input('crossfilter-yaxis-column', 'value'),
     dash.dependencies.Input('crossfilter-year--slider', 'value')])
def update_table(yaxis_column_name, year):
    dff = df[df['Month'] == year]
    fillter_list = yaxis_column_name
    if not type(yaxis_column_name) == list:
        fillter_list = [yaxis_column_name]

    dff = dff[dff.mac.isin(fillter_list)]
    dff = dff[['_id', 'mac', 'lat', 'lon', 'vreme']]
    return dff.to_dict('records')

# @app.callback(
#     dash.dependencies.Output)

@app.callback(
    dash.dependencies.Output('crossfilter-indicator-scatter', 'figure'),
    [dash.dependencies.Input('crossfilter-yaxis-column', 'value'),
     dash.dependencies.Input('crossfilter-year--slider', 'value')])
def update_graph(yaxis_column_name,
                 year_value):
    dff = df[df['Month'] == year_value]

    fillter_list = yaxis_column_name
    if not type(yaxis_column_name) == list:
        fillter_list = [yaxis_column_name]

    lat = dff[dff.mac.isin(fillter_list)]['lat']
    lon = dff[dff.mac.isin(fillter_list)]['lon']

    return {
        'data': [go.Scattermapbox(
            lat=lat,
            lon=lon,
            text=dff[dff.mac.isin(fillter_list)]['_id'],
            customdata=dff[dff.mac.isin(fillter_list)]['_id'],
            mode='markers',
            marker={
                'size': 15,
                'opacity': 0.5,
                'color': dff['Hour'],
                'colorscale': 'Hot',
                'showscale': True,
                'colorbar': dict(
                    thicknessmode="fraction",
                    title="Time of<br>Day",
                    x=0.935,
                    xpad=0,
                    nticks=24,
                    tickfont=dict(
                        color='white'
                    ),
                    titlefont=dict(
                        color='white'
                    ),
                    titleside='right'
                )
            }
        )],
        'layout': go.Layout(
            autosize=False,
            #     width=1024,
            height=800,
            mapbox=dict(
                accesstoken="pk.eyJ1IjoibWF0NzQ1IiwiYSI6ImNqdzkyb25rbTJuYno0N3BsYjQxcWlpMGsifQ.aMsNBfb9og0-iUuoTG9_ZQ",
                bearing=0,
                zoom=11,
                center=dict(lat=44.785,
                            lon=20.474),
                style='dark'),
            updatemenus=[
                dict(
                    buttons=([
                        dict(
                            args=[{
                                'mapbox.zoom': 11,
                                'mapbox.center.lat': '44.785',
                                'mapbox.center.lon': '20.4742',
                                'mapbox.bearing': 0,
                                'mapbox.style': 'dark'
                            }],
                            label='Reset Zoom',
                            method='relayout'
                        )
                    ]),
                    direction='left',
                    pad={'r': 0, 't': 0, 'b': 0, 'l': 0},
                    showactive=False,
                    type='buttons',
                    x=0.45,
                    xanchor='left',
                    yanchor='bottom',
                    bgcolor='#323130',
                    borderwidth=1,
                    bordercolor="#6d6d6d",
                    font=dict(
                        color="#FFFFFF"
                    ),
                    y=0.02
                ),
                dict(
                    buttons=([
                        dict(
                            args=[{
                                'mapbox.zoom': 16,
                                'mapbox.center.lon': '20.491262',
                                'mapbox.center.lat': '44.815144',
                                'mapbox.bearing': 0,
                                'mapbox.style': 'dark'
                            }],
                            label='Bogoslovija',
                            method='relayout'
                        ),
                        dict(
                            args=[{
                                'mapbox.zoom': 13,
                                'mapbox.center.lon': '20.4732',
                                'mapbox.center.lat': '44.77408',
                                'mapbox.bearing': 0,
                                'mapbox.style': 'dark'
                            }],
                            label='Bulevar Oslobodjenja',
                            method='relayout'
                        ),
                        dict(
                            args=[{
                                'mapbox.zoom': 17,
                                'mapbox.center.lon': '20.465170',
                                'mapbox.center.lat': '44.787711',
                                'mapbox.bearing': 0,
                                'mapbox.style': 'dark'
                            }],
                            label='Autkomanda',
                            method='relayout'
                        ),
                        dict(
                            args=[{
                                'mapbox.zoom': 15,
                                'mapbox.center.lon': '20.4024',
                                'mapbox.center.lat': '44.8263',
                                'mapbox.bearing': 0,
                                'mapbox.style': 'dark'
                            }],
                            label='Studentska',
                            method='relayout'
                        ), dict(
                            args=[{
                                'mapbox.zoom': 15,
                                'mapbox.center.lon': '20.475507',
                                'mapbox.center.lat': '44.772322',
                                'mapbox.bearing': 0,
                                'mapbox.style': 'dark'
                            }],
                            label='Jove Ilica',
                            method='relayout'
                        )
                    ]),
                    direction="down",
                    pad={'r': 0, 't': 0, 'b': 0, 'l': 0},
                    showactive=False,
                    bgcolor="rgb(50, 49, 48, 0)",
                    type='buttons',
                    yanchor='bottom',
                    xanchor='left',
                    font=dict(
                        color="#FFFFFF"
                    ),
                    x=0,
                    y=0.05

                ),

            ]

        )
    }


#First graph
def create_time_series(dff, title):
    dff = dff.reset_index()
    dff['freq'] = 0
    dff['amp'] = 0
    if not dff.empty:
        a = pd.DataFrame([x for x in json.loads(dff.loc[0, 'analiza']) for x in x.items()], columns=['freq', 'amp'])
        dff = a
    print(dff.to_string())
    return {
        'data': [go.Scatter(
            x=dff['freq'],
            y=dff['amp'],
            mode='lines+markers',
            line=dict(
                shape="spline",
                smoothing=1,
                width=1,
                color='#158db4'
            ),
        )],
        'layout': {
            'height': 400,
            'margin': {'l': 100, 'b': 30, 'r': 10, 't': 100},
            'annotations': [{
                'x': 0, 'y': 0.85, 'xanchor': 'left', 'yanchor': 'bottom',
                'xref': 'paper', 'yref': 'paper', 'showarrow': False,
                'align': 'left', 'bgcolor': 'rgba(0, 0, 0, 0.05)',
                'text': 'Mapa', 'font': {'color': '#34acd3', 'family': 'Courier New, monospace', 'size': 6}
            }],
            'xaxis': {'showgrid': False},
            'plot_bgcolor': 'rgba(0,0,0, 0.8)',
        }
    }


# First graph, hover over map
@app.callback(
    dash.dependencies.Output('y-time-series', 'figure'),
    [dash.dependencies.Input('crossfilter-indicator-scatter', 'hoverData'),
     dash.dependencies.Input('crossfilter-yaxis-column', 'value'),
     dash.dependencies.Input('dash-table', 'selected_row_ids') #property doesnt exist
     ])
def update_x_timeseries(hoverData, yaxis_column_name, selected_row):
    dff = df[df['_id'] == hoverData['points'][0]['customdata']]
    #    if not selected_row[0] is None:
    # dff = df.iloc[selected_row[0], :]

    #    dff = dff[dff.mac.isin([yaxis_column_name])]
    return create_time_series(dff, yaxis_column_name)

# Second graph
@app.callback(
    dash.dependencies.Output('x-time-series', 'figure'),
    [
        dash.dependencies.Input('crossfilter-yaxis-column', 'value'),
        dash.dependencies.Input('dash-table', 'selected_rows')
    ])
def update_second_plot(yaxis_column_name, selected_row):
    test = selected_row

    if test is None:
        return create_time(df, yaxis_column_name)

    dff = df.iloc[selected_row, :]

    #    dff = dff[dff.mac.isin([yaxis_column_name])]
    return create_time(dff, yaxis_column_name)

# Second graph
def create_time(dff, title):
    dff = dff.reset_index()
    dff['freq'] = 0
    dff['amp'] = 0
    if not dff.empty:
        # print (json.loads(dff.loc[0, 'analiza']))
        a = pd.DataFrame([x for x in json.loads(dff.loc[0, 'analiza']) for x in x.items()], columns=['freq', 'amp'])
        dff = a

    return {
        'data': [go.Scatter(
            x=dff['freq'],
            y=dff['amp'],
            mode='lines+markers',
            line=dict(
                shape="spline",
                smoothing=1,
                width=1,
                color='#CD3333'
            ),
        )],
        'layout': {
            'height': 400,
            'margin': {'l': 100, 'b': 100, 'r': 10, 't': 10},
            'annotations': [{
                'x': 0, 'y': 0.85, 'xanchor': 'left', 'yanchor': 'bottom',
                'xref': 'paper', 'yref': 'paper', 'showarrow': False,
                'align': 'left', 'bgcolor': 'rgba(0, 0, 0, 0.05)',
                'text': 'Tabela', 'font': {'color': '#c6342b', 'family': 'Courier New, monospace', 'size': 16}
            }],
            'xaxis': {'showgrid': False},
            'plot_bgcolor': 'rgba(0,0,0, 0.8)',
        }
    }


if __name__ == '__main__':
    app.run_server(debug=True)
